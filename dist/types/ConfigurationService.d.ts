export declare interface ApplicationSettingsConfiguration {
    name?: string;
    image?: string;
    description?: string;
    thumbnail?: string;
}
export declare interface RemoteSettingsConfiguration {
    server: string;
}
export declare interface LocalizationSettingsConfiguration {
    locales?: Array<string>;
    uiLocales?: Array<string>;
    defaultLocale?: string;
    append?: any;
}
export declare interface SettingsConfiguration {
    app?: ApplicationSettingsConfiguration;
    remote?: RemoteSettingsConfiguration;
    i18n?: LocalizationSettingsConfiguration;
    auth?: any;
}
export declare interface ApplicationConfiguration {
    settings: SettingsConfiguration;
}
export declare class ConfigurationService {
    configuration: ApplicationConfiguration;
    constructor(input?: ApplicationConfiguration);
    get settings(): SettingsConfiguration;
    get currentLanguage(): string | undefined;
    set currentLanguage(lang: string | undefined);
}
