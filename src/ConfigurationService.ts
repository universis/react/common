

export declare interface ApplicationSettingsConfiguration {
    name?: string;
    image?: string;
    description?: string;
    thumbnail?: string;
}

export declare interface RemoteSettingsConfiguration {
  server: string;
}

export declare interface LocalizationSettingsConfiguration {
  locales?: Array<string>;
  uiLocales?: Array<string>;
  defaultLocale?: string;
  append?: any;
}

export declare interface SettingsConfiguration {
  app?: ApplicationSettingsConfiguration;
  remote?: RemoteSettingsConfiguration;
  i18n?: LocalizationSettingsConfiguration;
  auth?: any;
}

export declare interface ApplicationConfiguration {
  settings: SettingsConfiguration;
}

/**
 * Application configuration service
 */
export class ConfigurationService {
    
    public configuration: ApplicationConfiguration;

    constructor(input?: ApplicationConfiguration) {
        this.configuration = input || { settings: {} };
    }

    /**
     * Get the application settings
     */
    get settings(): SettingsConfiguration {
        return this.configuration.settings;
    }

    /**
     * Get current language
     */
    get currentLanguage(): string | undefined {
        const value = localStorage.getItem("currentLang");
        return value || this.configuration.settings?.i18n?.defaultLocale;
    }

    /**
     * Set current language
     */
    set currentLanguage(lang: string) {
        const locale = this.configuration.settings?.i18n?.locales?.find((locale) => locale === lang);
        if (locale == null) {
            throw new Error('The given locale is unavailable');
        }
        localStorage.setItem("currentLang", locale);
    }

}