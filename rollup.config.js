const resolve = require('@rollup/plugin-node-resolve');
const commonjs = require('@rollup/plugin-commonjs');
const typescript = require('@rollup/plugin-typescript');;
const {terser} = require('rollup-plugin-terser');
const external = require('rollup-plugin-peer-deps-external');
const postcss = require('rollup-plugin-postcss');

const packageJson = require('./package.json');

module.exports = [
    {
        input: 'src/index.ts',
        output: [
            {
                file: packageJson.main,
                format: 'cjs',
                sourcemap: true
            },
            {
                file: packageJson.module,
                format: 'es',
                sourcemap: true
            }
        ],
        plugins: [
            external(),
            resolve(),
            typescript({
                tsconfig: './tsconfig.json'
            }),
            postcss(),
            terser()
        ]
    }
]